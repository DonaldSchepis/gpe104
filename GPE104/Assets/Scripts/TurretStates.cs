﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretStates : MonoBehaviour
{
    public enum AIState {Idle , Seek , Rest};
    public AIState aiState;

    // If the player is closer than this, we will chase
    public float aiSenseRadius;

    // if our health is below this, we will stop and heal
    public float cutoff;

    // in hp/framedraw  
    public float restingHealRate;

    // Get the player's transform
    public Transform target;

    // Get our transfrom
    public Transform tf;

    public float speed = 10.0f;

    // Enemy's max health
    public float maxHealth = 10.0f;

    // Enemy's current health
    public float health;

    // Start is called before the first frame update
    void Start()
    {
        // Set the current health at start to the max
        health = maxHealth;
    }

    public void DoIdle()
    {
        // Do Nothing!
    }

    public void DoSeek()
    {
        Vector3 vectorToTarget = target.position - tf.position;
        tf.position += vectorToTarget.normalized * speed;
    }
    public void DoRest()
    {
        // Increase our health. Remember that our increase in this case is "per frame"!
        health += restingHealRate;

        // But never go over our max health
        health = Mathf.Min(health, maxHealth);
    }

    public void ChangeState(AIState newState)
    {
        // Change our state
        aiState = newState;
    }

    // Update is called once per frame
    void Update()
    {
        if (aiState == AIState.Idle)
        {
            // Do Action
            DoIdle();
            // Check for transitions
            if (Vector3.Distance(transform.position, tf.position) < aiSenseRadius)
            {
                aiState = AIState.Seek;
            }
        }
        else if (aiState == AIState.Seek)
        {
            // Do Action
            DoSeek();
            // Check for transitions
            if (Vector3.Distance(transform.position, tf.position) > aiSenseRadius)
            {
                aiState = AIState.Idle;
            }
            if (health < cutoff)
            {
                aiState = AIState.Rest;
            }
        }
        else if (aiState == AIState.Rest)
        {
            // Do Action
            DoRest();
            // Check for transitions
            if (health > cutoff)
            {
                aiState = AIState.Idle;
            }
        }
    }
}
